#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

"""
Example Airflow DAG to submit Apache Spark applications using
`SparkSubmitOperator`, `SparkJDBCOperator` and `SparkSqlOperator`.
"""
import uuid
from datetime import datetime

from airflow.models import DAG

# from airflow.operators.bash import BashOperator
# from airflow.providers.apache.spark.operators.spark_jdbc import SparkJDBCOperator
# from airflow.providers.apache.spark.operators.spark_sql import SparkSqlOperator
# from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import (
    KubernetesPodOperator,
)

with DAG(
    dag_id="example_spark_operator",
    schedule_interval=None,
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=["example"],
) as dag:
    # [START howto_operator_spark_submit]

    # submit_job = SparkSubmitOperator(
    #     application="/opt/spark/examples/src/main/python/pi.py", task_id="submit_job"
    # )

    kubernetes_pod = KubernetesPodOperator(
        task_id="spark-job-task-ex",
        name="spark-job-task",
        namespace="default",
        is_delete_operator_pod=True,
        image="datadocker86/spark-py:v3.1.1",
        cmds=["/opt/spark/bin/spark-submit"],
        arguments=[
            "--master=k8s://https://1E71A49DF6C95E9849FA0C007F42120E.sk1.ap-south-1.eks.amazonaws.com:443",
            "--deploy-mode=cluster",
            "--name=spark-pi",
            "--class=org.apache.spark.examples.SparkPi",
            f"--conf=spark.kubernetes.driver.pod.name=spark-pi-driver.{uuid.uuid4().hex}",
            "--conf=spark.executor.instances=2",
            "--conf=spark.kubernetes.container.image=datadocker86/spark-py:v3.1.1",
            "--conf=spark.kubernetes.executor.deleteOnTermination=true",
            "local:////opt/spark/examples/jars/spark-examples_2.12-3.1.1.jar",
        ],
    )

    # submit_job = BashOperator(
    #     task_id='submit_job',
    #     bash_command='spark-submit --master spark://k8s-default-sparkmas-449b30f959-ac56e3e1bc966d97.elb.ap-south-1.amazonaws.com:7077 /opt/spark/examples/src/main/python/pi.py'
    #     # bash_command='spark-submit --master spark://{{ dag_run.conf["spark-master_host"] }}:{{ dag_run.conf["spark-master_port"] }} /opt/spark/examples/src/main/python/pi.py'
    # )

    # [END howto_operator_spark_submit]

    # [START howto_operator_spark_jdbc]
    # jdbc_to_spark_job = SparkJDBCOperator(
    #     cmd_type='jdbc_to_spark',
    #     jdbc_table="foo",
    #     spark_jars="/opt/spark/jars/postgresql-42.2.12.jar",
    #     jdbc_driver="org.postgresql.Driver",
    #     metastore_table="bar",
    #     save_mode="overwrite",
    #     save_format="JSON",
    #     task_id="jdbc_to_spark_job",
    # )

    # jdbc_to_spark_job = BashOperator(
    #     task_id='jdbc_to_spark_job',
    #     bash_command='spark-submit --master spark://devkraft:7077 --jars /opt/spark/jars/postgresql-42.2.12.jar --name airflow-spark-jdbc --queue root.default /home/dt00035/.local/lib/python3.8/site-packages/airflow/providers/apache/spark/hooks/spark_jdbc_script.py -cmdType jdbc_to_spark -metastoreTable bar -jdbcTable foo -jdbcDriver org.postgresql.Driver -saveMode overwrite -saveFormat JSON'
    # )

    # spark_to_jdbc_job = SparkJDBCOperator(
    #     cmd_type='spark_to_jdbc',
    #     jdbc_table="foo",
    #     spark_jars="/opt/spark/jars/postgresql-42.2.12.jar",
    #     jdbc_driver="org.postgresql.Driver",
    #     metastore_table="bar",
    #     save_mode="append",
    #     task_id="spark_to_jdbc_job",
    # )
    # # [END howto_operator_spark_jdbc]

    # # [START howto_operator_spark_sql]
    # sql_job = SparkSqlOperator(sql="SELECT * FROM bar", master="local", task_id="sql_job")
    # # [END howto_operator_spark_sql]

if __name__ == "__main__":
    dag.cli()
