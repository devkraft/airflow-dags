from airflow import DAG

from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import (
    KubernetesPodOperator,
)

from airflow.operators.python import PythonOperator
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from datetime import datetime, timedelta

import uuid
from io import StringIO
import os
import pandas as pd
import requests

aws_conn_id = "aws"
bucket = "angel-datalake-platform"

def upload_to_s3():
    """Grabs data from Covid endpoint and saves to flat file on S3"""
    # Connect to S3
    s3_hook = S3Hook(aws_conn_id=aws_conn_id)

    # Get data from API
    url = "https://covid19.who.int/"
    res = requests.get(url + "WHO-COVID-19-global-data.csv")

    # save data to CSV on S3
    s3_hook.load_string(
        res.text, "WHO-COVID-19-global-data.csv", bucket_name=bucket, replace=True
    )


def process_data():
    """Reads data from S3, processes, and saves to new S3 file"""
    # Connect to S3
    s3_hook = S3Hook(aws_conn_id=aws_conn_id)

    # Read data
    data = StringIO(
        s3_hook.read_key(key="WHO-COVID-19-global-data.csv", bucket_name=bucket)
    )
    df = pd.read_csv(data, sep=",")

    # Process data
    # Date_reported	Country_code	Country	WHO_region	New_cases	Cumulative_cases	New_deaths	Cumulative_deaths
    processed_data = df[["Country_code", "New_cases", "Cumulative_cases"]]
    

    # save processed data to CSV on S3
    s3_hook.load_string(
        processed_data.to_string(),
        "processed.csv",
        bucket_name=bucket,
        replace=True,
    )


def process_data_2():
    """Reads data from S3, processes, and saves to new S3 file"""
    # Connect to S3
    s3_hook = S3Hook(aws_conn_id=aws_conn_id)

    # Read data
    data = StringIO(
        s3_hook.read_key(key="processed.csv", bucket_name=bucket)
    )
    df = pd.read_csv(data, sep=",")

    # Process data
    # Date_reported	Country_code	Country	WHO_region	New_cases	Cumulative_cases	New_deaths	Cumulative_deaths
    processed_data = df[["Country_code", "New_cases", "Cumulative_cases"]]
    

    # save processed data to CSV on S3
    s3_hook.load_string(
        processed_data.to_string(),
        "processed_2.csv",
        bucket_name=bucket,
        replace=True,
    )


# Default settings applied to all tasks
default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "email_on_failure": False,
    "email_on_retry": False,
    # "retries": 1,
    # "retry_delay": timedelta(minutes=1),
}


with DAG(
    "intermediary_data_storage_dag",
    start_date=datetime(2021, 1, 1),
    max_active_runs=1,
    schedule_interval="@daily",
    default_args=default_args,
    catchup=False,
) as dag:

    generate_file = PythonOperator(
        task_id="generate_file",
        python_callable=upload_to_s3
    )

    process_data = PythonOperator(
        task_id="process_data",
        python_callable=process_data
    )
    
    process_data_2 = PythonOperator(
        task_id="process_data_2",
        python_callable=process_data_2
    )

    generate_file >> process_data >> process_data_2
